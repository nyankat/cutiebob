# CUTIEBOB

## Name
The Quick and Dirty Breakout Board (QDBOB, thence "CUTIEBOB") is a breakout board 
for AN12 and AN15 nixie display tubes made with KiCad 7.

![3D rendering showing a circuit board with a vacuum tube holder and a 0.1 inch 2x6 connector](/media/nixie\ breakout.jpg)

## Usage
If you want to just create copies of the board, download the gerbers folder as
a zip file and send it to your favourite PCB design house.

If you want to make changes to it, open it in KiCad 7.

## Support
Let me figure this out.

## Roadmap
Add support for several tubes on one board, and colon and decimal tubes.

## Authors and acknowledgment
Kat Stark, with much guidance from John Holmes regarding the functioning of the AN12. 

## License
BSD licensed.

## Project status
Active, but slow.